import List from "../structure/List";

import Fish from "../modules/Fish";
import Bird from "../modules/Bird";
import Insect from "../modules/Insect";

export default class Actions {
  public fillList(list: List, lines: string[]): void {
    lines.filter((item) => item !== "");

    lines.forEach((item) => {
      if (item.includes("Add Fish")) {
        const name = item.split(" ")[2];
        const livingPlace = item.split(" ")[3];
        const fish = new Fish(name, livingPlace);
        list.add(fish);
      }

      if (item.includes("Add Bird")) {
        const name = item.split(" ")[2];
        const flyRelation = item.split(" ")[3];
        const bird = new Bird(name, Boolean(flyRelation));
        list.add(bird);
      }

      if (item.includes("Add Insect")) {
        const name = item.split(" ")[2];
        const numberOfLegs = item.split(" ")[3];
        const insect = new Insect(name, Number(numberOfLegs));
        list.add(insect);
      }
    });
  }

  public sortListAcending(list: List): List {
    const array: any = list.toArray();
    const sorted = new List();

    array.sort((a, b) => a.name - b.name);
    array.forEach((item) => sorted.add(item));

    return sorted;
  }

  public sortListDecending(list: List): List {
    const array: any = list.toArray();
    const sorted = new List();

    array.sort((a, b) => b.name - a.name);
    array.forEach((item) => sorted.add(item));

    return sorted;
  }

  public fileProgram(list: List, lines: string[]): void {
    this.fillList(list, lines);

    lines.forEach((item) => {
      if (item === "Print") {
        console.log("--- PRINT LIST ---");
        list.print();
      }

      if (item === "Sort name acending") {
        console.log("--- ACENDING SORT ---");
        list = this.sortListAcending(list);
      }

      if (item === "Sort name decending") {
        console.log("--- DECENDING SORT ---");
        list = this.sortListDecending(list);
      }

      if (item === "Remove list first") {
        console.log("--- REMOVE FIRST ---");
        list.removeFirst();
      }

      if (item === "Remove list last") {
        console.log("--- REMOVE LAST ---");
        list.removeLast();
      }

      if (item.includes("Remove typeof")) {
        console.log("--- REMOVE TYPEOF ---");
        const type = item.split(" ")[2];
        list.removeByType(type);
      }

      if (item.includes("Remove name")) {
        console.log("--- REMOVE BY NAME ---");
        list.removeByName(item.split(" ")[2]);
      }
    });
  }
}
