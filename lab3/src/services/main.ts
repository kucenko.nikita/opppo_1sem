import * as fs from "fs";

import List from "../structure/List";

import Actions from "./Actions";

export default function main() {
  const animals = new List();
  const data = fs.readFileSync("input.txt", "utf8");
  const actions = new Actions();

  actions.fileProgram(animals, data.split("\n"));
}
