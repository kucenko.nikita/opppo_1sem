import Bird from ".././modules/Bird";
import Fish from ".././modules/Fish";
import Insect from ".././modules/Insect";

export default class Node {
  public value: Bird | Fish | Insect;
  public next: Node;

  constructor(value: Bird | Fish | Insect) {
    this.value = value;
    this.next = null;
  }
}
