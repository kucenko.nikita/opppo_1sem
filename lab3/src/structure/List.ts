import Node from "./Node";

import Bird from ".././modules/Bird";
import Fish from ".././modules/Fish";
import Insect from ".././modules/Insect";

export default class List {
  private _head: Node;
  private _tail: Node;
  private _size: number;

  constructor() {
    this._head = null;
    this._tail = null;
    this._size = 0;
  }

  public get head(): Node {
    return this._head;
  }

  public get tail(): Node {
    return this._tail;
  }

  public get size(): number {
    return this._size;
  }

  public print(): void {
    let current = this._head;

    while (current !== null) {
      console.log(current.value);

      current = current.next;
    }
  }

  public add(value: Fish | Bird | Insect): void {
    const node = new Node(value);

    if (this._head === null) {
      this._head = node;

      this._tail = node;
    } else {
      this._tail.next = node;

      this._tail = node;
    }

    this._size++;
  }

  public removeFirst(): void {
    if (this._head === null) {
      return;
    }

    this._head = this._head.next;
  }

  public removeLast(): void {
    if (this._head === null) {
      return;
    }

    let current = this._head;

    while (current.next.next !== null) {
      current = current.next;
    }

    current.next = null;
  }

  public removeByType(value: string) {
    if (this._head === null) {
      return;
    }

    if (this._head.value.getType() === value) {
      this._head = this._head.next;

      this._size--;

      return;
    }

    let current = this._head;

    while (current.next !== null) {
      if (this._head.value.getType() === value) {
        current.next = current.next.next;

        this._size--;

        return;
      }

      current = current.next;
    }
  }

  public removeByName(value: string): void {
    if (this._head === null) {
      return;
    }

    if (this._head.value.getName() === value) {
      this._head = this._head.next;

      this._size--;

      return;
    }

    let current = this._head;

    while (current.next !== null) {
      if (current.next.value.getName() === value) {
        current.next = current.next.next;

        this._size--;

        return;
      }

      current = current.next;
    }
  }

  // public contains(value: number): boolean {
  //   let current = this._head;
  //
  //   while (current !== null) {
  //     if (current.value === value) {
  //       return true;
  //     }
  //
  //     current = current.next;
  //   }
  //
  //   return false;
  // }

  public clear(): void {
    this._head = null;

    this._tail = null;

    this._size = 0;
  }

  public toArray(): number[] {
    const array = [];

    let current = this._head;

    while (current !== null) {
      array.push(current.value);

      current = current.next;
    }

    return array;
  }
}
