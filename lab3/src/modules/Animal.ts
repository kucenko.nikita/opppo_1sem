export default interface Animal {
  name: string;
  type: string;
}
