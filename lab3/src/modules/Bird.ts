import Animal from "./Animal";

export default // @ts-ignore
class Bird implements Animal {
  private name: string;
  private readonly type: string;
  private flyRelation: boolean;

  constructor(name: string, flyRelation: boolean) {
    this.name = name;
    this.type = "Bird";
    this.flyRelation = flyRelation;
  }

  public getName(): string {
    return this.name;
  }

  public getType(): string {
    return this.type;
  }

  public getFlyRelation(): boolean {
    return this.flyRelation;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public setFlyRelation(flyRelation: boolean): void {
    this.flyRelation = flyRelation;
  }
}
