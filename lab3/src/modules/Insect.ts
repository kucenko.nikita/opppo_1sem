import Animal from "./Animal";

export default // @ts-ignore
class Insect implements Animal {
  private name: string;
  private readonly type: string;
  private numberOfLegs: number;

  constructor(name: string, numberOfLegs: number) {
    this.name = name;
    this.type = "Insect";
    this.numberOfLegs = numberOfLegs;
  }

  public getName(): string {
    return this.name;
  }

  public getType(): string {
    return this.type;
  }

  public getNumberOfLegs(): number {
    return this.numberOfLegs;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public setNumberOfLegs(numberOfLegs: number): void {
    this.numberOfLegs = numberOfLegs;
  }
}
