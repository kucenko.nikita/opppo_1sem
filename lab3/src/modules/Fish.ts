import Animal from "./Animal";

export default // @ts-ignore
class Fish implements Animal {
  private name: string;
  private readonly type: string;
  private livingPlace: string;
  private livingPlaces: string[] = ["Ocean", "Lake", "River"];

  constructor(name: string, livingPlace: string) {
    this.name = name;
    this.type = "Fish";
    if (!this.livingPlaces.includes(livingPlace)) {
      this.livingPlaces.push(livingPlace);
      this.livingPlace = livingPlace;
    }
  }

  public getName(): string {
    return this.name;
  }

  public getType(): string {
    return this.type;
  }

  public getLivingPlace(): string {
    return this.livingPlace;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public setLivingPlace(livingPlace: string): void {
    this.livingPlace = livingPlace;
  }
}
