PRINT

ADD Fish ('Karas', 'river')
ADD Fish ('Karas', 'lake')

ADD Bird ('Eagle', true)
ADD Bird ('Golub', false)

ADD Insect ('Spider', 8)
ADD Insect ('Scolopendra', 40)

PRINT

SORT name DESC
PRINT
SORT name ASC
PRINT

REMOVE FIRST
REMOVE LAST
PRINT
REMOVE type Fish
PRINT

REMOVE number_of_legs 8

REMOVE name Eagle
PRINT
REMOVE name Karas
PRINT
REMOVE name Bob

PRINT