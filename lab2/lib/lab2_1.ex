defmodule LAB2 do
  def main do
    read_file("input.txt")
    |> clear
    |> parse
    |> process
  end

  def clear(input) do
    input
    |> Enum.with_index(1)
    |> Enum.map(fn {line, index} -> {index, line} end)
    |> Enum.map(fn {i, line} -> {i, String.split(line, "//") |> hd} end) # separate command from comment
    |> Enum.map(fn {i, line} -> {i, String.trim(line)} end) # remove whitespace
    |> Enum.filter(fn {_, line} -> line != "" end) # remove empty lines
  end

  def parse(input) do
    input
    |> Enum.map(fn {i, line} -> {i, Regex.named_captures(~r/^(?<command>\S+)\s*(?<arguments>.*)?$/, line)} end)
  end

  def process(commands, animal \\ [])
  def process(commands, animal) when length(commands) == 0, do: animal
  def process([{i, %{"command" => command, "arguments" => arguments}} | restCommands], animal) do
    IO.puts("Line #{i}: #{command} #{arguments}")

    animal = try do
      case command do
        "ADD" ->
          add_args = Command.parse_add(arguments)
          IO.inspect(add_args)
          Command.add(animal, add_args)
        "PRINT" -> Command.print(animal)
        "SORT" ->
          sort_args = Command.parse_sort(arguments)
          Command.sort(animal, sort_args)
        "REMOVE" -> 
          remove_args = Command.parse_remove(arguments)
          Command.remove(animal, remove_args)
        _ ->
          IO.puts("Unknown command: #{command}")
          animal
      end
    rescue
      e in ArgumentError ->
        IO.puts("ArgumentError: " <> e.message)
        animal
    end
    # IO.inspect(animal, label: "animal is ")
    process(restCommands, animal)
  end

  def read_file(filename) do
    File.read!(filename)
    |> String.split("\n")
  end
end
