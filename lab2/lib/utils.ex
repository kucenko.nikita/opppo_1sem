defmodule Utils do
  def tryParseInt(value) do
    cond do
      is_binary(value) -> Integer.parse(value) |> case do
        { parsed_value, _ } -> parsed_value
        _ -> :error
      end
      is_integer(value) -> value
      true -> :error
    end
  end
end
