defmodule Command do
  def print(animal) do
    IO.inspect(
      animal,
      list: true,
      pretty: true,
      syntax_colors: [
        atom: :cyan,
        boolean: :magenta,
        charlist: :yellow,
        nil: :magenta,
        number: :yellow,
        string: :green
    ])
    animal
  end

  def parse_add(arguments) do
    %{
      "animal_type" => animal_type,
      "arguments" => arguments
    } = Regex.named_captures(~r/^(?<animal_type>\S+)\s*\((?<arguments>.*)?\)$/, arguments)

    params = String.split(arguments, ~r/,\s+/)
    |> Enum.map(&String.replace(&1, ~r/'/, ""))
    |> Enum.map(&String.trim(&1))

    { animal_type, params }
  end

  def add(animal, { animal_type, params }) when animal_type === "Insect" do
    animal = animal ++ [(params |> List.to_tuple |> Model.Insect.new)]
    animal
  end

  def add(animal, { animal_type, params }) when animal_type === "Fish" do
    animal = animal ++ [(params |> List.to_tuple |> Model.Fish.new)]
    animal
  end

  def add(animal, { animal_type, params }) when animal_type === "Bird" do
    animal = animal ++ [(params |> List.to_tuple |> Model.Bird.new)]
    animal
  end

  def add(_, { animal_type, _ }) do
    raise ArgumentError, message: "Unknown animal type: #{animal_type}"
  end

  def parse_remove(arguments) do
    %{
      "property" => property,
      "value" => value
    } = Regex.named_captures(~r/^(?<property>\S+)\s*(?<value>\S+)?$/, arguments)

    { property, value }
  end

  def remove(animal, { property, value }) do
    animal = case property do
        "FIRST" -> Enum.drop(animal, 1)
        "LAST" -> Enum.drop(animal, -1)
        _ -> Enum.reject(animal, &("#{&1[String.to_atom(property)]}" == "#{value}"))
    end
    animal
  end

  def parse_sort(arguments) do
    %{
      "property" => property,
      "direction" => direction
    } = Regex.named_captures(~r/^(?<property>\S+)\s*(?<direction>\S+)?$/, arguments)

    direction = case direction do
      "ASC" -> :asc
      "DESC" -> :desc
      "" -> :asc
      _ -> raise ArgumentError, message: "Unknown sort direction. Should be either ASC or DESC, but got #{direction}"
    end

    { property, direction }
  end

  def sort(animal, { property, direction }) do
    animal = Enum.sort_by(animal, &(&1[String.to_atom(property)]), direction)
    animal
  end
end
