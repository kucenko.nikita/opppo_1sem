defmodule Model.Animal do
  def new({ name }) do
    {
      name
    } = validateConstructorParameters(name)

    %{
      type: "Animal",
      name: name
    }
  end

  def validateConstructorParameters(name) do

    if !String.valid?(name) do
      raise ArgumentError, message: "name must be a string, but got #{name}"
    end

    { name }
  end
end
