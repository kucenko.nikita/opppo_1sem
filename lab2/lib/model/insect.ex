defmodule Model.Insect do
  def new({ name, number_of_legs }) do
    { number_of_legs } = validateConstructorParameters(number_of_legs)


    Map.merge(
      Model.Animal.new({
        name
      }),
      %{
        type: "Insect",
        number_of_legs: number_of_legs
      }
    )
  end

  def validateConstructorParameters(number_of_legs) do
    parsed_number_of_legs = Utils.tryParseInt(number_of_legs)

    if !is_integer(parsed_number_of_legs) || parsed_number_of_legs < 1 do
      raise ArgumentError, message: "number_of_legs must be a positive number above 0, but got #{number_of_legs}"
    end

    { parsed_number_of_legs }
  end
end
