defmodule Model.Bird do
  def new({ name, fly_relation }) do

    Map.merge(
      Model.Animal.new({
        name
      }),
      %{
        type: "Bird",
        fly_relation: fly_relation
      }
    )
  end

end
