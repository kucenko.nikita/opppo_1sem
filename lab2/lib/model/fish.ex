defmodule Model.Fish do
  def new({ name, place_of_living }) do
    { place_of_living } = validateConstructorParameters(place_of_living)

    Map.merge(
      Model.Animal.new({
        name
      }),
      %{
        type: "Fish",
        place_of_living: place_of_living
      }
    )
  end

  def validateConstructorParameters(place_of_living) do

    if !String.valid?(place_of_living) do
      raise ArgumentError, message: "place_of_living must be a string, but got #{place_of_living}"
    end

    { place_of_living }
  end
end
