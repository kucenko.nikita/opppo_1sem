import Node from "./Node.js";

export default class List {
  constructor() {
    this.head = null;
    this.tail = null;
    this.size = 0;
  }

  getHead() {
    return this.head;
  }

  getTail() {
    return this.tail;
  }

  getSize() {
    return this.size;
  }

  print() {
    let current = this.head;

    while (current !== null) {
      console.log(current.value);

      current = current.next;
    }
  }

  add(value) {
    const node = new Node(value);

    if (this.head === null) {
      this.head = node;

      this.tail = node;
    } else {
      this.tail.next = node;

      this.tail = node;
    }

    this.size++;
  }

  removeFirst() {
    if (this.head === null) {
      return;
    }

    this.head = this.head.next;
  }

  removeLast() {
    if (this.head === null) {
      return;
    }

    let current = this.head;

    while (current.next.next !== null) {
      current = current.next;
    }

    current.next = null;
  }

  removeByType(value) {
    if (this.head === null) {
      return;
    }

    if (this.head.value.type === value) {
      this.head = this.head.next;

      this.size--;

      return;
    }

    let current = this.head;

    while (current.next !== null) {
      if (this.head.value.type === value) {
        current.next = current.next.next;

        this.size--;

        return;
      }

      current = current.next;
    }
  }

  removeByName(value) {
    if (this.head === null) {
      return;
    }

    if (this.head.value.name === value) {
      this.head = this.head.next;

      this.size--;

      return;
    }

    let current = this.head;

    while (current.next !== null) {
      if (current.next.value.name === value) {
        current.next = current.next.next;

        this.size--;

        return;
      }

      current = current.next;
    }
  }

  toArray() {
    const array = [];

    let current = this.head;

    while (current !== null) {
      array.push(current.value);

      current = current.next;
    }

    return array;
  }
}
