import fs from "fs";

import List from ".././structure/List.js";

import { fileProgram } from "./actions.js";

export function main() {
  const list = new List();
  const data = fs.readFileSync("input.txt", "utf8");

  fileProgram(list, data.split("\n"));
}
