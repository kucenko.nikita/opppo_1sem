import List from "../structure/List.js";

export function fillList(list, lines) {
  lines.filter((item) => item !== "");

  lines.forEach((item) => {
    if (item.includes("Add Fish")) {
      const name = item.split(" ")[2];
      const livingPlace = item.split(" ")[3];
      const fish = { name: name, type: "Fish", livingPlace: livingPlace };
      list.add(fish);
    }

    if (item.includes("Add Bird")) {
      const name = item.split(" ")[2];
      const flyRelation = item.split(" ")[3];
      const bird = { name: name, type: "Bird", flyRelation: flyRelation };
      list.add(bird);
    }

    if (item.includes("Add Insect")) {
      const name = item.split(" ")[2];
      const numberOfLegs = item.split(" ")[3];
      const insect = { name: name, type: "Insect", numberOfLegs: numberOfLegs };
      list.add(insect);
    }
  });
}

function sortListAcending(list) {
  const array = list.toArray();
  const sorted = new List();

  array.sort((a, b) => a.name - b.name);
  array.forEach((item) => sorted.add(item));

  return sorted;
}

function sortListDecending(list) {
  const array = list.toArray();
  const sorted = new List();

  array.sort((a, b) => b.name - a.name);
  array.forEach((item) => sorted.add(item));

  return sorted;
}

export function fileProgram(list, lines) {
  fillList(list, lines);

  lines.forEach((item) => {
    if (item === "Print") {
      console.log("--- PRINT LIST ---");
      list.print();
    }

    if (item === "Sort name acending") {
      console.log("--- ACENDING SORT ---");
      list = sortListAcending(list);
    }

    if (item === "Sort name decending") {
      console.log("--- DECENDING SORT ---");
      list = sortListDecending(list);
    }

    if (item === "Remove list first") {
      console.log("--- REMOVE FIRST ---");
      list.removeFirst();
    }

    if (item === "Remove list last") {
      console.log("--- REMOVE LAST ---");
      list.removeLast();
    }

    if (item.includes("Remove typeof")) {
      console.log("--- REMOVE TYPEOF ---");
      const type = item.split(" ")[2];
      list.removeByType(type);
    }

    if (item.includes("Remove name")) {
      console.log("--- REMOVE BY NAME ---");
      list.removeByName(item.split(" ")[2]);
    }
  });
}
